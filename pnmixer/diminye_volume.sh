#!/bin/dash

notify_p=$(if
           	[ "$1" = + ]
           then
           	pulsemixer --change-volume +5
           elif
           	[ "$1" = - ]
           then
           	pulsemixer --change-volume -5
           elif
           	[ "$1" = tog ]
           then
           	pulsemixer --toggle-mute
           else
           	echo "false"
           fi)


sleep 0.03

if
	[ "$notify_p" != "false" ]
then
	level=$(pulsemixer --get-volume | cut -d \  -f 1)
	nIcon=$(if
	        	[ $(pulsemixer --get-mute) = 0 ]
	        then
	        	case $level in
	        		[0-9]|[1-2][0-9]|3[0-3])
	        			echo "audio-volume-low-symbolic.symbolic"
	        			;;
	        		3[4-9]|[4-5][0-9]|6[0-7])
	        			echo "audio-volume-medium-symbolic.symbolic"
	        			;;
	        		6[8-9]|[7-9][0-9]|100)
	        			echo "audio-volume-high-symbolic.symbolic"
	        			;;
	        		*)
	        			echo "audio-volume-overamplified-symbolic.symbolic"
	        			;;
	        	esac
	        else
	        	echo "audio-volume-muted-symbolic.symbolic"
	        fi)
	bar=$(seq -s "━" $(echo "scale = 2; $level / 2.30" | bc | awk '{printf("%d\n",$1 - 0.5)}') | sed 's/[0-9]//g')

	notify-send "$(if [ "$bar" = "" ]; then echo " "; else echo $bar; fi)" -i "$nIcon" -h string:x-canonical-private-synchronous:volume
fi
