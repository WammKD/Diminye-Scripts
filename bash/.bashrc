# ~/.bashrc: executed by bash(1) for non-login shells.
# see /usr/share/doc/bash/examples/startup-files (in the package bash-doc)
# for examples

# If not running interactively, don't do anything
case $- in
	*i*)
		;;
	*)
		return
		;;
esac

# don't put duplicate lines or lines starting with space in the history.
# See bash(1) for more options
HISTCONTROL=ignoreboth

# append to the history file, don't overwrite it
shopt -s histappend

# for setting history length see HISTSIZE and HISTFILESIZE in bash(1)
HISTSIZE=1000
HISTFILESIZE=2000

# check the window size after each command and, if necessary,
# update the values of LINES and COLUMNS.
shopt -s checkwinsize

# If set, the pattern "**" used in a pathname expansion context will
# match all files and zero or more directories and subdirectories.
#shopt -s globstar

# make less more friendly for non-text input files, see lesspipe(1)
#[ -x /usr/bin/lesspipe ] && eval "$(SHELL=/bin/sh lesspipe)"

# set variable identifying the chroot you work in (used in the prompt below)
if
	[ -z "${debian_chroot:-}" ] && [ -r /etc/debian_chroot ]
then
	debian_chroot=$(cat /etc/debian_chroot)
fi

# set a fancy prompt (non-color, unless we know we "want" color)
case "$TERM" in
	xterm-color|*-256color)
		color_prompt=yes
		;;
esac

# uncomment for a colored prompt, if the terminal has the capability; turned
# off by default to not distract the user: the focus in a terminal window
# should be on the output of commands, not on the prompt
#force_color_prompt=yes

if
	[ -n "$force_color_prompt" ]
then
	if
		[ -x /usr/bin/tput ] && tput setaf 1 >&/dev/null
	then
		# We have color support; assume it's compliant with Ecma-48
		# (ISO/IEC-6429). (Lack of such support is extremely rare, and such
		# a case would tend to support setf rather than setaf.)
		color_prompt=yes
	else
		color_prompt=
  fi
fi

if [ "$color_prompt" = yes ]; then
	PROMPT_COMMAND="__prompt_command${PROMPT_COMMAND}"

	__prompt_command() {
		local      Exit="$?"

		local color_beg="\[\033["
		local      Bold="${color_beg}1;"
		local      Norm="${color_beg}0;"
		local       Bla="30m\]"
		local       Red="31m\]"
		local       Gre="32m\]"
		local       Yel="33m\]"
		local       Blu="34m\]"
		local       Pur="35m\]"
		local       Cya="36m\]"
		local Light_Gra="37m\]"
		local  Dark_Gra="90m\]"
		local Light_Red="91m\]"
		local Light_Gre="92m\]"
		local Light_Yel="93m\]"
		local Light_Blu="94m\]"
		local Light_Mag="95m\]"
		local Light_Cya="96m\]"
		local       Whi="97m\]"

		local char_part="\[\033("
		local  char_beg="${char_part}0\]"
		local  char_end="${char_part}B\]"
		local  top_left="${char_beg}l${char_end}"
		local  mid_left="${char_beg}t${char_end}"
		local  bot_left="${char_beg}m${char_end}"
		local    hyphen="${char_beg}q${char_end}"

		local    number=0

		while
			[ "$number" -le 0 ]
		do
			number=$RANDOM

			let "number %= 4"
		done

		if
			[ $Exit != 0 ]
		then
			# {ಥ_ಥ} # {╥﹏╥} #

			local eye_color="${Bold}${Blu}"

			if
				[ $number -eq 3 ]
			then
				local   Eye="${eye_color}╥"
				local Mouth="﹏"
			else
				local   Eye="${eye_color}ಥ"
				local Mouth="_"
			fi
		else
			# {^‿^} # {˘v˘} #

			local eye_color="${Bold}${Whi}"

			if
				[ $number -eq 3 ]
			then
				local   Eye="${eye_color}˘"
				local Mouth="v"
			else
				local   Eye="${eye_color}^"
				local Mouth="‿"
			fi
		fi
		local Face="${Eye}${Norm}${Light_Gra}${Mouth}${Eye}${Bold}${Dark_Gra}"

		git branch &>/dev/null

		if
			[ $? -eq 0 ]
		then
			local git_status="${Bold}${Dark_Gra}["

			git status | grep "nothing to commit" > /dev/null 2>&1

			if
				[ $? -eq 0 ]
			then
				git_status+="${Bold}${Yel}"
			else
				git_status+="${Bold}${Red}"
			fi

			git_status+="$(git rev-parse --abbrev-ref HEAD)"  # branch name

			set -- $(git rev-list --left-right --count $(git for-each-ref --format="%(upstream:short)" $(git symbolic-ref -q HEAD))..HEAD)

			local  ahead=$2
			local behind=$1

			if
				[ $(($ahead + $behind)) -gt 0 ]
			then
				git_status+=" "
			fi

			if
				[ $behind -gt 0 ]
			then
				git_status+="↑${ahead}"
			fi

			if
				[ $behind -gt 0 ]
			then
				git_status+="↓${behind}"
			fi

			git_status+="${Bold}${Dark_Gra}]${hyphen}"
		else
			local git_status=""
		fi

		local Wd="${Bold}${Pur}$PWD"
		if
			[ "$PWD" != "/" ]
		then
			Wd+="/"
		fi
		Wd+="${Bold}${Dark_Gra}"


		 PS1="\n${Bold}${Dark_Gra}${top_left}(${Wd})${hyphen}{${Face}}"
		PS1+="\n${Bold}${Dark_Gra}${mid_left}${git_status}(${Bold}${Whi}$(echo -e "$(la -1 | wc -l)" | tr -d "[[:space:]]") ${Norm}${Light_Gra}file(s), ${Bold}${Whi}$(ls -lah | grep -m 1 total | sed "s/total //" | sed "s/[a-z]//")${Norm}${Light_Gra}iB${Bold}${Dark_Gra})${hyphen}(${Bold}${Cya}\u${Bold}${Whi}@${Bold}${Gre}\h${Bold}${Dark_Gra})${hyphen}(${Norm}${Light_Gra}jobs:${Bold}${Whi}\j${Bold}${Dark_Gra})"
		PS1+="\n${Bold}${Dark_Gra}${bot_left}${hyphen}>${Norm}${Whi} "
	}
else
	PS1='${debian_chroot:+($debian_chroot)}\u@\h:\w\$ '
fi
unset color_prompt force_color_prompt

# If this is an xterm set the title to user@host:dir
case "$TERM" in
	xterm*|rxvt*)
		PS1="\[\e]0;${debian_chroot:+($debian_chroot)}\u@\h: \w\a\]$PS1"
		;;
	*)
		;;
esac

# enable color support of ls and also add handy aliases
if
	[ -x /usr/bin/dircolors ]
then
	test -r ~/.dircolors && eval "$(dircolors -b ~/.dircolors)" || eval "$(dircolors -b)"
	alias ls='ls --color=auto'
	#alias dir='dir --color=auto'
	#alias vdir='vdir --color=auto'

	#alias grep='grep --color=auto'
	#alias fgrep='fgrep --color=auto'
	#alias egrep='egrep --color=auto'
fi

# colored GCC warnings and errors
#export GCC_COLORS='error=01;31:warning=01;35:note=01;36:caret=01;32:locus=01:quote=01'

# some more ls aliases
alias ll='ls -l'
alias la='ls -A'
#alias l='ls -CF'

# Alias definitions.
# You may want to put all your additions into a separate file like
# ~/.bash_aliases, instead of adding them here directly.
# See /usr/share/doc/bash-doc/examples in the bash-doc package.
if
	[ -f ~/.bash_aliases ]
then
	. ~/.bash_aliases
fi

# enable programmable completion features (you don't need to enable
# this, if it's already enabled in /etc/bash.bashrc and /etc/profile
# sources /etc/bash.bashrc).
if
	! shopt -oq posix
then
	if
		[ -f /usr/share/bash-completion/bash_completion ]
	then
		. /usr/share/bash-completion/bash_completion
	elif
		[ -f /etc/bash_completion ]
	then
		. /etc/bash_completion
	fi
fi

source /etc/profile.d/undistract-me.sh
