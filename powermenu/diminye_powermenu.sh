#!/usr/bin/env bash

theme="$1"

shutdown="Shut Down"
reboot="Restart"
lock="Lock Screen"
logout="Log Off"

confirm_exit() {
	ans=$(echo -e "No\nYes" | rofi -dmenu                          \
													       -theme "confirm_or_deny_$theme" \
													       -u     1                        \
													       -i                              \
													       -p     "Are you sure?" &)

	if
		[[ $ans == "Yes" ]]
	then
		# possible future reference: https://github.com/ardadem/graceful-shutdown/blob/master/graceful_shutdown.sh
		wmctrl -c '' # Tell everyone to close gracefully
		while
			[[ $(wmctrl -l | grep -v tint2 | wc -l) -gt 0 ]] # Loop until all windows are closed
		do
			sleep 0.5 # Wait a little bit before checking again
		done

		$1
	else
		diminye_powermenu $theme
	fi
}

case $(echo -e "$logout\n$lock\n$reboot\n$shutdown" | rofi -dmenu -theme "powermenu_$theme" -p "  Power Off" -i -u 3) in
	$shutdown)
		confirm_exit "systemctl poweroff"
		;;
	$reboot)
		confirm_exit "systemctl reboot"
		;;
	$lock)
		if
			[[ -f /usr/bin/xflock4 ]] || [[ -f /usr/local/bin/xflock4 ]]
		then
			xflock4
		elif
			[[ -f /usr/bin/i3lock ]]
		then
			i3lock
		elif
			[[ -f /usr/bin/betterlockscreen ]]
		then
			betterlockscreen -l
		fi
		;;
	$logout)
		confirm_exit "$(if
		                	[[ "$DESKTOP_SESSION" == "Openbox" ]]
		                then
		                	echo "openbox --exit"
		                elif
		                	[[ "$DESKTOP_SESSION" == "bspwm"   ]]
		                then
		                	echo "bspc quit"
		                elif
		                	[[ "$DESKTOP_SESSION" == "i3"      ]]
		                then
		                	echo "i3-msg exit"
		                fi)"
		;;
esac
