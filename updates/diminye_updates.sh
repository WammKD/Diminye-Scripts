#!/bin/dash

if
	[ "$1" = update ]
then
	x-terminal-emulator -e dash -c "sleep 0.1 && wmctrl -r :ACTIVE: -b add,fullscreen && sudo apt-get update && sudo apt-get upgrade && lemon_updates.sh" &
else
	if
		[ "$1" = dist ]
	then
		x-terminal-emulator -e dash -c "sleep 0.1 && wmctrl -r :ACTIVE: -b add,fullscreen && sudo apt-get update && sudo apt-get dist-upgrade && lemon_updates.sh" &
	else
		updates=$(apt-get -s upgrade | awk '/^Inst/ { print $2 }' | wc -l)
		upgrades=$(apt-get -s dist-upgrade | awk '/^Inst/ { print $2 }' | wc -l)

		if
			[ $upgrades -ne 0 ]
		then
			notify-send "Software Updates Available"                      \
			            "$(if
			               	[ -f /var/run/reboot-required ]
			               then
			               	echo "To see changes, reboot after install"
			               else
			               	echo "$(if
			               	        	[ $updates -eq 0 ]
			               	        then
			               	        	echo $upgrades
			               	        else
			               	        	echo $updates
			               	        fi) new system updates are available"
			               fi)"                                           \
			            -i "$(if
			                  	[ -f /var/run/reboot-required ]
			                  then
			                  	echo "gtk-dialog-warning"
			                  else
			                  	echo "gtk-dialog-info"
			                  fi)"                                        \
			            -h string:x-canonical-private-synchronous:updates
		fi

		echo "/usr/share/icons/Diminye/status/512/"$(if
		                                             	[ $upgrades -ne 0 ]
		                                             then
		                                             	if
		                                             		[ $updates -eq 0 ]
		                                             	then
		                                             		echo "security-high-symbolic.symbolic.png"
		                                             	else
		                                             		echo "security-low-symbolic.symbolic.png"
		                                             	fi
		                                             else
		                                             	echo ""
		                                             fi)
		echo $(if
		       	[ $upgrades -ne 0 ]
		       then
		       	if [ $updates -eq 0 ]; then echo $upgrades; else echo $updates; fi
		       else
		       	echo ""
		       fi)
	fi
fi
