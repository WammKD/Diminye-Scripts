#!/bin/dash

cpu=$(printf "%.2f\n" $(awk -v a="$(awk '/cpu /{print $2+$4,$2+$4+$5}' /proc/stat; sleep 0.3)" '/cpu /{split(a,b," "); print 100*($2+$4-b[1])/($2+$4+$5-b[2])}'  /proc/stat))%
#cpu=$(top -b -n2 -p 1 | fgrep "Cpu(s)" | tail -1 | awk -F'id,' -v prefix="$prefix" '{ split($1, vs, ","); v=vs[length(vs)]; sub("%", "", v); printf "%s%.1f%%\n", prefix, 100 - v }')
ram=$(free | awk '/Mem/{printf("%.2f%"), $3/$2*100}')
swap=$(free | awk '/Swap/{printf("%.2f%"), $3/$2*100}')

#printf "%s\n" "🖵    $cpu       🐏   $ram       🖴    $swap"
printf "%s\n" "   $cpu           $ram           $swap"
