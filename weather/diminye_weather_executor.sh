#!/bin/dash

data=$(wget https://wttr.in/?format="%c_%t_%m_%p_%w" 2>/dev/null -O - | sed 's/+//g')
icon=$(case $(echo $data | cut -d_ -f1) in
       	"⛅️ ")
       		echo $(case $(date '+%H') in
       		       	0[0-6]|19|2[0-9])
       		       		echo "weather-few-clouds-night-symbolic.symbolic.png"
       		       		;;
       		       	*)
       		       		echo "weather-few-clouds-symbolic.symbolic.png"
       		       		;;
       		       esac)
       		;;
       	"🌫 ")
       		echo "weather-fog-symbolic.symbolic.png"
       		;;
       	"🌦")
       		echo "weather-showers-scattered-symbolic.symbolic.png"
       		;;
       	"☀️")
       		echo $(case $(date '+%H') in
       		       	0[0-6]|19|2[0-9])
       		       		echo "weather-clear-night-symbolic.symbolic.png"
       		       		;;
       		       	*)
       		       		echo "weather-clear-symbolic.symbolic.png"
       		       		;;
       		       esac)
       		;;
       	"☁️")
       		echo "weather-overcast-symbolic.symbolic.png"
       		;;
       	"⛈"|"🌧")
       		echo "weather-showers-symbolic.symbolic.png"
       		;;
       	"🌩"|"🌩 ")
       		echo "weather-storm-symbolic.symbolic.png"
       		;;
       	"❄️"|"🌨 ")
       		echo "weather-snow-symbolic.symbolic.png"
       		;;
       	*)
       		echo "computer-fail-symbolic.symbolic.png"
       		;;
       esac)
text=$(result=$(echo $data | cut -d_ -f2)

       if
       	[ ${#result} -gt 6 ]
       then
       	echo "--°F"
       else
       	echo $result
       fi)

echo "/usr/share/icons/Diminye/status/512/$icon"
echo "$text"
