#!/bin/bash
# This script owes a lot to CRubuntuNG for insight about how to approach this
# https://alkusin.net/crubuntung

# Avoid repeated requests for a password
grab_sudo() {
	sudo -v

	while
		[ true ]
	do
		sudo -n true

		sleep 45

		kill -0 "$$" || exit
	done 2>/dev/null &
}
notify_user() {
	echo -e "$1...\n\n"

	sleep 3s
}

# Grab the password for the first (and only) time
grab_sudo

clear
	notify_user "Setting swapiness to 10 and disabling install suggestions and recommendations"
		echo "vm.swappiness=10" | sudo tee -a /etc/sysctl.conf
		echo -e "APT::Install-Suggests \"0\";\nAPT::Install-Recommends \"0\";" | sudo tee /etc/apt/apt.conf.d/98disable-suggests-and-recommends

clear
	notify_user "Updating and upgrading apt"
		sudo apt -y update
		sudo apt -y upgrade

clear
	notify_user "Accepting Microsoft font license"  # In case it's ever made use of
		echo ttf-mscorefonts-installer msttcorefonts/accepted-mscorefonts-eula select true | sudo debconf-set-selections

clear
	notify_user "Installing packages"
		sudo apt-get install --no-install-recommends -y software-properties-common
		sudo apt-get install --no-install-recommends -y dirmngr gpg gpg-agent  # For add-apt-repository

		sudo sed -i 's/ main$/ main contrib non-free/g'              /etc/apt/sources.list

		sudo add-apt-repository --enable-source -y ppa:ubuntubudgie/backports
		sudo sed -i 's/^deb /# deb /'                                /etc/apt/sources.list.d/ubuntubudgie-ubuntu-backports-*.list
		sudo sed -i 's/\/ubuntu [a-z]\+ main$/\/ubuntu focal main/g' /etc/apt/sources.list.d/ubuntubudgie-ubuntu-backports-*.list
		sudo apt-key adv --keyserver keyserver.ubuntu.com --recv-key 237694670F1068D89D7EA18BBB53052A24597C20

		sudo add-apt-repository --enable-source -y ppa:papirus/papirus
		sudo sed -i 's/^deb /# deb /'                                /etc/apt/sources.list.d/papirus-ubuntu-papirus-*.list
		sudo sed -i 's/\/ubuntu [a-z]\+ main$/\/ubuntu focal main/g' /etc/apt/sources.list.d/papirus-ubuntu-papirus-*.list
		sudo apt-key adv --keyserver keyserver.ubuntu.com --recv-key E58A9D36647CAE7F

		sudo apt-get                            -y update

		packagelist=(build-essential  # For make, gcc

		             # ubuntu-drivers-common
		             libavcodec-extra       # Allows things like watching Netflix

		             ### notifications
		             dunst
		             libnotify-bin
		             undistract-me

		             ### internets
		             network-manager

		             ### compression
		             zip unzip
		             rar unrar
		             p7zip-full

		             ### misc. utilities
		             ca-certificates
		             wget
		             x11-utils          # xev,xfontsel,xkill,xprop,etc.
		             x11-xserver-utils  # xrandr,xset
		             mesa-utils         # glxinfo for inxi; it's only 137kB
		             wmctrl             # for use with deklanche
		             xdotool            # for use with deklanche
		             xsel               # for use with clipmenu
		             bc                 # for use with diminye_volume.sh
		             bash-completion    # self-explanatory
		             command-not-found
		             git

		             ### desktop utilities/general-uses
		             compton
		             inxi                       # For system info.; it's only 638kB
		             xdg-user-dirs
		             xdg-utils
		             arandr
		             gsimplecal
		             policykit-1-gnome          # To be able to use policykit app.s (e.g. pkexec)
		             gtk-update-icon-cache
		             # gtk3-nocsd
		             ttf-ancient-fonts
		             fonts-cantarell            # General   font
		             fonts-firacode             # Monospace font
		             gnome-themes-standard
		             papirus-icon-theme
		             # libreoffice-style-papirus – built, now; see below
		             lxappearance
		             lxappearance-obconf
		             ncdu                       # terminal disk usage analyzer
		             # baobab                     # Disk Usage Analyzer
		             # software-properties-gtk    # GUI for update settings and drivers finder
		             gnome-keyring
		             gnome-keyring-pkcs11       # For a certificate database
		             # libpam-gnome-keyring       # Hopefully handle login unlocking
		             seahorse                   # GUI
		             libsecret-1-dev            # For Git functionality

		             ### desktop "panels"
		             tint2

		             ### audio
		             pulseaudio
		             pulsemixer
		             pnmixer

		             ### X11
		             openbox
		             xserver-xorg

		             ### applications
		             lxterminal
		             htop
		             rofi
		             i3lock
		             # xautolock
		             # bluez                # bluetooth
		             # btscanner            # bluetooth
		             nitrogen
		             gdebi
		             viewnior
		             file-roller
		             gnome-calculator
		             gnome-font-viewer
		             gnome-screenshot
		             xfce4-power-manager
		             eject
		             gvfs                   # For external drives
		             gvfs-backends          # For archive, ftp, http, mpt, smb…
		             gvfs-bin               # Basically, remote network access in Thunar
		             exfat-fuse
		             exfat-utils
		             dosfstools             # fat16/32
		             ntfs-3g
		             tumbler                # To generate image thumbnails
		             thunar
		             thunar-archive-plugin
		             thunar-gtkhash
		             thunar-volman
		             catfish
		             caffeine)
		# Commented out elements are things I'm uncertain about
		sudo apt-get install --no-install-recommends -y ${packagelist[@]}
		sudo update-command-not-found

		# Setup checking for package updates, each day
		echo -e '#!/bin/sh\n\napt-get update' | sudo tee /etc/cron.daily/apt-update
		sudo chmod 755 /etc/cron.daily/apt-update

		# Setup Git with Gnome Keyring
		cd /usr/share/doc/git/contrib/credential/libsecret
		sudo make
		git config --global credential.helper /usr/share/doc/git/contrib/credential/libsecret/git-credential-libsecret
		cd /tmp

		# Setup Gnome-Keyring for automatic unlock on login
		#sudo sed -i 's/auth       optional   pam_group.so/auth       optional   pam_group.so\nauth       optional   pam_gnome_keyring.so/' /etc/pam.d/login
		#sudo sed -i 's/session    optional   pam_keyinit.so force revoke/session    optional   pam_keyinit.so       force revoke\nsession    optional   pam_gnome_keyring.so auto_start/' /etc/pam.d/login
		#sudo sed -i 's/@/password      optional   pam_gnome_keyring.so\n\n@/' /etc/pam.d/passwd

		# mkdir -p ~/.local/share/applications
		# cp /usr/share/applications/rxvt-unicode.desktop ~/.local/share/applications
		# sed -i 's/urxvt_48x48.xpm/\/usr\/share\/icons\/Papirus\/64x64\/apps\/xterm.svg/g' ~/.local/share/applications/rxvt-unicode.desktop

		cd /tmp
		sudo apt-get install --no-install-recommends -y devscripts
		                                                debhelper
		                                                libimlib2-dev
		                                                libxext-dev
		                                                libxinerama-dev
		                                                libxcomposite-dev
		                                                libxdamage-dev
		                                                libxfixes-dev
		                                                libxmu-dev
		                                                libfontconfig1-dev
		                                                libxft-dev
		                                                libxrender-dev

		apt-get source --build skippy-xd
		sudo dpkg --install skippy-xd_*.deb

		# apt-get source --build libreoffice-style-papirus
		# sudo dpkg --install libreoffice-style-papirus_*.deb

		sudo apt-get purge -y devscripts
		                      debhelper
		                      libimlib2-dev
		                      libxext-dev
		                      libxinerama-dev
		                      libxcomposite-dev
		                      libxdamage-dev
		                      libxfixes-dev
		                      libxmu-dev
		                      libfontconfig1-dev
		                      libxft-dev
		                      libxrender-dev
		sudo apt-get autoremove

		cd /tmp

		wget https://github.com/cdown/clipnotify/archive/1.0.2.tar.gz
		tar -zxvf 1.0.2.tar.gz
		cd clipnotify-1.0.2
		sudo apt-get install --no-install-recommends -y libxfixes-dev
		make
		sudo mv clipnotify /usr/bin
		sudo apt-get purge -y libxfixes-dev
		sudo apt-get autoremove -y

		cd /tmp

		wget https://github.com/cdown/clipmenu/archive/6.2.0.tar.gz
		tar -zxvf 6.2.0.tar.gz
		cd clipmenu-6.2.0
		sudo mv clip* /usr/bin

		cd /tmp

		wget https://github.com/cylgom/ly/releases/download/v0.5.0/ly_0.5.0.zip
		unzip ly_0.5.0.zip
		cd ly_0.5.0
		sudo ./install.sh
		sudo systemctl enable ly.service

		cd /tmp

		mkdir -p ~/.local/share/wallpapers
		mkdir -p ~/.config/nitrogen
		echo -e "[geometry]\nposx=429\nposy=124\nsizex=500\nsizey=500\n\n[nitrogen]\nview=icon\nrecurse=true\nsort=alpha\nicon_cap=false\ndirs=$HOME/.local/share/wallpapers;" > ~/.config/nitrogen/nitrogen.cfg
		wget https://github.com/elementary/wallpapers/archive/master.tar.gz
		tar -zxvf master.tar.gz
		rm master.tar.gz
		cp wallpapers-master/backgrounds/*.jpg ~/.local/share/wallpapers
		echo -e "[xin_-1]\nfile=$HOME/.local/share/wallpapers/Sunset by the Pier.jpg\nmode=0\nbgcolor=#E8E8E7" > ~/.config/nitrogen/bg-saved.cfg

		cd /tmp

		git clone https://codeberg.org/Jaft/diminye-icons.git
		cd diminye-icons
		git submodule update --init --recursive
		sudo apt-get install graphicsmagick-imagemagick-compat gtk-3-examples
		./generate-symbolics.sh
		sudo apt-get purge graphicsmagick-imagemagick-compat gtk-3-examples
		sudo apt-get autoremove
		sudo rm -r .git
		cd ..
		sudo mv diminye-icons /usr/share/icons/Diminye

		mkdir -p ~/.local/bin
		wget https://codeberg.org/Jaft/diminye-scripts/archive/master.tar.gz
		tar -zxvf master.tar.gz
		cd diminye-scripts
		### Aerosnap
		sudo cp aerosnap/deklanche /usr/local/bin
		### Bash
		cp bash/.bashrc ~/
		### Clipmenu
		sudo cp clipmenu/clipmenu.desktop /usr/share/applications
		sudo cp clipmenu/clipmenuDel.sh   /usr/local/bin
		### Dunst
		mkdir -p ~/.config/dunst
		cp dunst/dunstrc ~/.config/dunst/
		### GSimpleCal
		mkdir -p ~/.config/gsimplecal
		cp gsimplecal/config ~/.config/gsimplecal/
		### GTK Settings
		mkdir -p ~/.config/gtk-3.0
		     cp    gtk/settings.ini ~/.config/gtk-3.0/
		echo -e "file://$HOME/Documents\nfile://$HOME/Downloads\nfile://$HOME/Games\nfile://$HOME/Music\nfile://$HOME/Pictures\nfile://$HOME/tmp\nfile://$HOME/Videos" > ~/.config/gtk-3.0/bookmarks

		     cp    gtk/.gtkrc-2.0   ~/

		sudo cp -r gtk/openbox-3    /usr/share/themes/Adwaita
		### i3lock
		sudo cp i3lock/xflock4 /usr/local/bin
		### Keyboard
		# sudo sed -i 's/XKBOPTIONS=""/XKBOPTIONS="compose:ralt"/g' /etc/default/keyboard
		### LXTerminal
		mkdir -p ~/.config/lxterminal
		cp lxterminal/lxterminal.conf ~/.config/lxterminal
		### Network Manager
		sudo cp network_manager/tz-update /usr/local/bin

		echo 'ALL ALL=(root) NOPASSWD: /usr/local/bin/tz-update' | sudo EDITOR='tee -a' visudo

		echo '[ "$2" = "up" ] && sudo tz-update &' | sudo tee /etc/NetworkManager/dispatcher.d/99-tzupdate
		sudo chmod +x /etc/NetworkManager/dispatcher.d/99-tzupdate
		### Openbox
		mkdir -p ~/.config/openbox
		cp openbox/* ~/.config/openbox/
		### Performance Monitoring
		sudo cp performance/diminye_performance_executor.sh /usr/local/bin
		### PNmixer
		mkdir -p ~/.config/pnmixer
		cp pnmixer/config ~/.config/pnmixer/

		sudo cp pnmixer/diminye_volume.sh /usr/local/bin

		sudo rm /usr/share/pnmixer/pnmixer-high.png
		sudo ln -s /usr/share/icons/Diminye/status/512/audio-volume-high-symbolic.symbolic.png /usr/share/pnmixer/pnmixer-high.png

		sudo rm /usr/share/pnmixer/pnmixer-low.png
		sudo ln -s /usr/share/icons/Diminye/status/512/audio-volume-low-symbolic.symbolic.png /usr/share/pnmixer/pnmixer-low.png

		sudo rm /usr/share/pnmixer/pnmixer-medium.png
		sudo ln -s /usr/share/icons/Diminye/status/512/audio-volume-medium-symbolic.symbolic.png /usr/share/pnmixer/pnmixer-medium.png

		sudo rm /usr/share/pnmixer/pnmixer-muted.png
		sudo ln -s /usr/share/icons/Diminye/status/512/audio-volume-muted-symbolic.symbolic.png /usr/share/pnmixer/pnmixer-muted.png
		### Power
		sudo cp powermenu/diminye_powermenu.sh /usr/local/bin
		### Rofi
		mkdir -p ~/.config/rofi
		cp rofi/* ~/.config/rofi/
		### Skippy-XD
		mkdir -p ~/.config/skippy-xd
		cp skippy-xd/* ~/.config/skippy-xd/
		### System Info.
		sudo cp system_info/system_info.sh      /usr/local/bin
		sudo cp system_info/system_info.desktop /usr/share/applications
		### Update Monitor
		sudo cp updates/diminye_updates.sh /usr/local/bin
		### Viewnior
		sudo sed -i 's/Exec=viewnior %F/Exec=viewnior --fullscreen %F/g' /usr/share/applications/viewnior.desktop
		### Thunar
		mkdir -p ~/.config/Thunar
		cp thunar/* ~/.config/Thunar/
		### Tint2
		mkdir -p ~/.config/tint2
		cp tint2/* ~/.config/tint2/
		### Toggle Swap
		sudo cp ram_management/toggle_swap.sh /usr/local/bin
		### Weather
		sudo cp weather/weather.desktop      /usr/share/applications
		sudo cp weather/diminye_weather_*.sh /usr/local/bin
		### Xdefaults
		cp urxvt/.Xdefaults ~
		### XDG
		cp xdg/user-dirs.dirs ~/.config

		mkdir -p ~/Desktop
		mkdir -p ~/Downloads
		mkdir -p ~/Games
		cp -r Templates ~/
		mkdir -p ~/Public
		mkdir -p ~/Documents
		mkdir -p ~/Music
		mkdir -p ~/Pictures
		ln -s /tmp/ ~/tmp
		mkdir -p ~/Videos

		cd /tmp

clear
	notify_user "Disabling checking for internet connection on boot"
		sudo systemctl disable systemd-networkd-wait-online.service
		sudo systemctl mask    systemd-networkd-wait-online.service

clear
	notify_user "Setting openbox as x-session-manager"
		sudo update-alternatives --set x-session-manager /usr/bin/openbox-session

clear
	read -p "Installation is finished. Please press ENTER to reboot."
		systemctl reboot
